
/*
*
* Identify orthologs 
*
*/
//  To run locally

//  Also Download Slnky
// TODO:
// wget https://github.com/slncky/slncky/archive/refs/heads/master.zip
// unzip master.zip
// rm master.zip

// Download dependencies
// From  https://www.dropbox.com/s/jifnrr3kjq95w64/annotations.tar.gz?dl=0 into slinky master dir
// tar -xf annotations.tar.gz
// rm annotations.tar.gz

// Also TODO
// cp /home/luisas/Desktop/cluster/data/01_bulk_RNA-Seq_lncRNAs_annotation/00_preliminaryfiles/rheMac10.fa ./slncky-master/annotations/
// cp /home/luisas/Desktop/cluster/data/01_bulk_RNA-Seq_lncRNAs_annotation/00_preliminaryfiles/rheMac10.fa.fai ./slncky-master/annotations/
// cp /home/luisas/Desktop/cluster/data/01_bulk_RNA-Seq_lncRNAs_annotation/00_preliminaryfiles/rheMac10ToHg38.over.chain.gz ./slncky-master/annotations/
// cp /home/luisas/Desktop/cluster/data/01_bulk_RNA-Seq_lncRNAs_annotation/00_preliminaryfiles/annotations.config ./slncky-master/

params.dirData = "/home/luisas/Desktop/cluster/data/"
params.slnkydir = "/home/luisas/Desktop/cluster/data/99_BroadAnnotation_Feb2021/03_novel_lncRNAs_list/04_orthologs/slncky-master/"
Channel.fromPath("${params.dirData}/99_BroadAnnotation_Feb2021/03_novel_lncRNAs_list/rheMac10_EBOV_and_novel_genenames.gtf")
       .into{ refFullChannel; refFullChannel2;  }

sourcefile = Channel.fromPath("${params.dirData}/01_bulk_RNA-Seq_lncRNAs_annotation/00_preliminaryfiles/ensemblSource.txt").collect()

prep_subfiles = Channel.fromPath("${baseDir}/scripts/createSubfiles.R").collect()
convert =  Channel.fromPath("${baseDir}/scripts/convert_annotation.R").collect()
gtfToGenePred = Channel.fromPath("${baseDir}/scripts/gtfToGenePred").collect()
genePredToBed = Channel.fromPath("${baseDir}/scripts/genePredToBed").collect()
chr = Channel.fromPath("/home/luisas/Desktop/cluster/data/99_BroadAnnotation_Feb2021/03_novel_lncRNAs_list/04_orthologs/slncky-master/annotations/chromosomes_in_fasta.tsv").collect()

params.prefix = "rheMac10"

params.out_preliminary = "${params.dirData}/99_BroadAnnotation_Feb2021/03_novel_lncRNAs_list/04_orthologs"
// 1 . Create other subfiles (Separate gtfs for separate genes biotypes )


process Prepare_Subfiles{
  publishDir "${params.slnkydir}/annotations", mode: 'copy', overwrite: false
  input:
  file prep_subfiles
  file reffull from refFullChannel
  file gtfToGenePred
  file genePredToBed
  file sourcefile
  file chr

  output:
  file "*" into prep_subfiles_channel
  val "done" into prepdone
  file "rheMac10_EBOV_and_novel_genenames_UCSC.contigsfiltered.gtf" into refChannelContigfilter

  script:
  """
  Rscript ${prep_subfiles} ${reffull} ${sourcefile} "." ${params.prefix} ${chr}
  ./gtfToGenePred ${params.prefix}.ensGene.lnc.gtf ${params.prefix}.ensGene.lnc.genepred
  ./gtfToGenePred ${params.prefix}.ensGene.lnc.gtf ${params.prefix}.ensGene.lnc.genepred
  ./genePredToBed ${params.prefix}.ensGene.lnc.genepred ${params.prefix}.ensGene.lnc.bed

  ./gtfToGenePred ${params.prefix}.ensGene.mirna.gtf ${params.prefix}.ensGene.mirna.genepred
  ./genePredToBed ${params.prefix}.ensGene.mirna.genepred ${params.prefix}.ensGene.mirna.bed

  ./gtfToGenePred ${params.prefix}.ensGene.PC.gtf ${params.prefix}.ensGene.PC.genepred
  ./genePredToBed ${params.prefix}.ensGene.PC.genepred ${params.prefix}.ensGene.PC.bed

  ./gtfToGenePred ${params.prefix}.ensGene.Pseudogenes.gtf ${params.prefix}.ensGene.Pseudogenes.genepred
  ./genePredToBed ${params.prefix}.ensGene.Pseudogenes.genepred ${params.prefix}.ensGene.Pseudogenes.bed

  ./gtfToGenePred ${params.prefix}.ensGene.PC.gtf ${params.prefix}.ensGene.PC.genepred
  ./genePredToBed ${params.prefix}.ensGene.PC.genepred ${params.prefix}.ensGene.PC.bed

  ./gtfToGenePred ${params.prefix}.ensGene.snorna.gtf ${params.prefix}.ensGene.snorna.genepred
  ./genePredToBed ${params.prefix}.ensGene.snorna.genepred ${params.prefix}.ensGene.snorna.bed
  """
}

process convert{

  storeDir "${params.slnkydir}"

  input:
  file gtfToGenePred
  file genePredToBed
  file reffull from refChannelContigfilter


  output:
  file "*" into conversions
  file "${reffull.baseName}.bed" into ref_contigsfiltered_bed

  script:
  """
  ./gtfToGenePred ${reffull} ${reffull.baseName}.genepred
  ./genePredToBed ${reffull.baseName}.genepred ${reffull.baseName}.bed
  """

}

//./slncky.v1.0 -2 /home/luisas/Desktop/cluster/data/01_bulk_RNA-Seq_lncRNAs_annotation/05_orthologs/slncky-master/rheMac10_EBOV_and_novel_genenames_UCSC.contigsfiltered.bed mmul10 out

// For the moment leave out of pipeline and run manually - on a hurry :S
// process slnky{
//
//   publishDir "${params.slnkydir}", mode: 'copy', overwrite: true
//
//   input:
//   file dirslnky
//   file config
//   file bed from ref_contigsfiltered_bed
//   val done from prepdone
//   file annot
//   file chain
//
//   output:
//   file "*" into slnkyoutput
//
//   script:
//   """
//   cd ${dirslnky}
//   ./slncky.v1.0 -c ${config} -2  ${bed} mmul10 out
//   """
// }
